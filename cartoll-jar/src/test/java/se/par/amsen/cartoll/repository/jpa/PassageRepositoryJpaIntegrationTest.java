package se.par.amsen.cartoll.repository.jpa;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import se.par.amsen.cartoll.domain.TaxInterval;
import se.par.amsen.cartoll.domain.TollStation;
import se.par.amsen.cartoll.domain.statistic.SimpleStatistic;
import se.par.amsen.cartoll.domain.vehicle.Vehicle;
import se.par.amsen.cartoll.repository.AdressRepository;
import se.par.amsen.cartoll.repository.OwnerRepository;
import se.par.amsen.cartoll.repository.TollStationRepository;
import se.par.amsen.cartoll.repository.VehicleRepository;
import se.par.amsen.cartoll.repository.mock.MockInitializer;
import se.par.amsen.cartoll.service.PassageService;
import se.par.amsen.cartoll.service.TaxIntervalService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/application_context_mock_jpa.xml"})
public class PassageRepositoryJpaIntegrationTest {
	
	Logger log = Logger.getLogger(PassageRepositoryJpaIntegrationTest.class);
	
	@Autowired
	PassageService passageServ;
	
	@Autowired
	OwnerRepository ownerRepo;
	
	@Autowired
	AdressRepository adressRepo;
	
	@Autowired
	VehicleRepository vehicleRepo;
	
	@Autowired
	TollStationRepository tollStationRepo;
	
	@Autowired
	@Qualifier("initializer")
	MockInitializer mock = new MockInitializer();
	
	@Autowired
	private TaxIntervalService taxIntervalServ;
	
	TollStation tollStation1;
	TollStation tollStation2;
	Vehicle vehicle;
	
	@Before
	public void setUp() throws Exception {
		passageServ.clearAllPassages();
		vehicleRepo.clearAllVehicles();
		ownerRepo.clearAllOwners();
		tollStationRepo.clearAllTollStations();
		adressRepo.clearAllAdresses();

		mock.init();
		
		vehicle = mock.getCar1();
		tollStation1 = mock.getTollStation1();
		tollStation2 = mock.getTollStation2();
	}
	
	@After
	public void tearDown() {
		passageServ.clearAllPassages();
		vehicleRepo.clearAllVehicles();
		ownerRepo.clearAllOwners();
		tollStationRepo.clearAllTollStations();
		adressRepo.clearAllAdresses();

	}

	@Test
	public void testGetPassagesForTollStation() {	
		assertEquals("Get Passages for TollStation 1", 8, passageServ.getPassagesForTollStation(tollStation1.getId()).size());
		assertEquals("Get Passages for TollStation 2", 7, passageServ.getPassagesForTollStation(tollStation2.getId()).size());
	}

	@Test
	public void testGetStatisticsByTollStationId() {
		assertEquals("Statistics for TollStation 2", 957, passageServ.getStatisticsByTollStationId(tollStation1.getId()).getTax());
	}

	@Test
	public void testGetStatisticsSummaryForAllTollStations() {
		assertEquals("Statistics summary for TollStations",1907, passageServ.getStatisticsSummaryForAllTollStations().getTax());
	}

	@Test
	public void testGetStatisticsByVehicleId() {
		SimpleStatistic statisticsByVehicleId = passageServ.getStatisticsByVehicleId(vehicle.getId());
		assertEquals("Sum of tax for Vehicle (Car 1)", 8, statisticsByVehicleId.getTax());
		assertEquals("Amount of passages for Vehicle (Car 1)", 2, statisticsByVehicleId.getPassages());
	}
	
	@Test
	public void testGetStatisticsWithNoPassages() {
		passageServ.clearAllPassages();
		passageServ.getStatisticsByTollStationId(1);
	}

}
