package se.par.amsen.cartoll.domain.vehicle;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import se.par.amsen.cartoll.domain.Owner;
import se.par.amsen.cartoll.domain.vehicle.Taxi;

public class TaxiTest {
	private final static String REG_NUMBER = "ABC-123";
	private final static boolean ENVIRO_CERT = true;
	private final static Owner OWNER = new Owner(null,null,null,null,null);

	private Taxi taxi; 

	@Before
	public void setup() {
		taxi = new Taxi(REG_NUMBER, OWNER, ENVIRO_CERT);
	}

	@Test
	public void testConstructor() {
		assertEquals("Registration Number", REG_NUMBER, taxi.getRegNumber());
		assertEquals("Owner", OWNER, taxi.getOwner());
		assertEquals("Environment Certificate", true, taxi.hasEnvironmentCertificate());
	}
}
