package se.par.amsen.cartoll.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TollStationTest {
	private final static String NAME = "Station-123";
	private final static Adress ADRESS = new Adress(null,null,null);
	private TollStation tollStation; 
	
	@Before
	public void setup() {
		tollStation = new TollStation(NAME, ADRESS);
	}
	
	@Test
	public void testConstructor() {
		assertEquals("Name", NAME, tollStation.getName());
		assertEquals("Adress", ADRESS, tollStation.getAdress());
	}
}
