package se.par.amsen.cartoll.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

/**
 * TollStation holds the data for a TollStation, a name and an Adress object.
 * @author Pär
 *
 */
@Entity
@NamedQueries(value={	@NamedQuery(name="getAllTollStations", query="SELECT t FROM TollStation t ORDER BY name ASC"),
						@NamedQuery(name="clearAllTollStations", query="DELETE FROM TollStation")})
public class TollStation {
	
	@Id
	@GeneratedValue
	private long id;
	
	private String name;
	
	@OneToOne(cascade=CascadeType.MERGE, fetch=FetchType.EAGER)
	private Adress adress;

	public TollStation(String name, Adress adress) {
		this.name = name;
		this.adress = adress;
	}
	
	public TollStation() { }
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Adress getAdress() {
		return adress;
	}

	public void setAdress(Adress adress) {
		this.adress = adress;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}	
}
