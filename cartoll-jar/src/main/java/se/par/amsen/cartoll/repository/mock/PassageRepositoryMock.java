package se.par.amsen.cartoll.repository.mock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import se.par.amsen.cartoll.domain.Passage;
import se.par.amsen.cartoll.domain.statistic.SimpleStatistic;
import se.par.amsen.cartoll.domain.statistic.TollStationStatistic;
import se.par.amsen.cartoll.repository.PassageRepository;

public class PassageRepositoryMock extends GenerateUniqueIdMock implements PassageRepository{
	private Map<Long,Passage> passages;

	public PassageRepositoryMock() {
		passages = new HashMap<Long,Passage>();
	}

	@Override
	public long createPassage(Passage passage) {
		passage.setId(generateUniqueId());
		passages.put(passage.getId(), passage);
		return passage.getId();
	}

	@Override
	public List<Passage> getPassages() {
		List<Passage> passagessTemp = new ArrayList<Passage>();
		passagessTemp.addAll(passages.values());
		return passagessTemp;
	}
	
	@Autowired(required=false)
	@Qualifier("passageMock")
	public void setPassages(ArrayList<Passage> passageList) {
		Map<Long, Passage> passages = new HashMap<Long, Passage>();
		
		for(Passage passage : passageList) {
			passages.put(passage.getId(), passage);
		}
		
		this.passages = passages;
	}

	@Override
	public Passage getPassageById(long id) {
		return passages.get(id);
	}

	@Override
	public long updatePassage(Passage passage) {
		if(passages.containsKey(passage.getId())) {
			passages.put(passage.getId(), passage);
			return passage.getId();
		}
		return -1;
	}

	@Override
	public boolean removePassageById(long id) {
		return passages.remove(id) != null ? true : false;
	}

	@Override
	public List<Passage> getPassagesForTollStation(long tollStationId) {
		List<Passage> passagesForTollStation = new ArrayList<Passage>();
		for(Entry<Long, Passage> entry : passages.entrySet()) {
			if(entry.getValue().getTollStation().getId() == tollStationId)
			passagesForTollStation.add(entry.getValue());
		}

		return passagesForTollStation;
	}

	@Override
	public TollStationStatistic getStatisticsByTollStationId(long tollStationId) {
		TollStationStatistic statistic = new TollStationStatistic(tollStationId);

		for(Passage passage : getPassagesForTollStation(tollStationId)) {
			statistic.setPassages(statistic.getPassages() + 1);
			statistic.setTax(statistic.getTax() + passage.getTax());
		}
		
		return statistic;
	}

	@Override
	public SimpleStatistic getStatisticsSummaryForAllTollStations() {
		SimpleStatistic statistic = new SimpleStatistic();

		for(Entry<Long, Passage> entry : passages.entrySet()) {
			statistic.setPassages(statistic.getPassages() + 1);
			statistic.setTax(statistic.getTax() + entry.getValue().getTax());
		}
		return statistic;
	}

	@Override
	public SimpleStatistic getStatisticsByVehicleId(long id) {
		
		//get passages for vehicle
		List<Passage> passagesForVehicle = new ArrayList<Passage>();
		for(Entry<Long, Passage> entry : passages.entrySet()) {
			if(entry.getValue().getVehicle().getId() == id)
				passagesForVehicle.add(entry.getValue());
		}
		
		SimpleStatistic statistic = new SimpleStatistic();

		for(Passage passage : passagesForVehicle) {
			statistic.setPassages(statistic.getPassages() + 1);
			statistic.setTax(statistic.getTax() + passage.getTax());
		}
		
		return statistic;
	}

	@Override
	public void clearAllPassages() {
		passages.clear();
		
	}
}
