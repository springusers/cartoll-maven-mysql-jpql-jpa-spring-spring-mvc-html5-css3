package se.par.amsen.cartoll.repository;

import java.util.List;

import se.par.amsen.cartoll.domain.Passage;
import se.par.amsen.cartoll.domain.statistic.SimpleStatistic;
import se.par.amsen.cartoll.domain.statistic.TollStationStatistic;

public interface PassageRepository {
	public long createPassage(Passage passage);
	public long updatePassage(Passage passage);
	public List<Passage> getPassages();
	public Passage getPassageById(long id);
	public boolean removePassageById(long id);
	
	public List<Passage> getPassagesForTollStation(long id);
	
	public TollStationStatistic getStatisticsByTollStationId(long id);
	public SimpleStatistic getStatisticsSummaryForAllTollStations();
	public SimpleStatistic getStatisticsByVehicleId(long id);
	public void clearAllPassages();
}
