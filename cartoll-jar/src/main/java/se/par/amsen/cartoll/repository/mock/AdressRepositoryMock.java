package se.par.amsen.cartoll.repository.mock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import se.par.amsen.cartoll.domain.Adress;
import se.par.amsen.cartoll.repository.AdressRepository;

public class AdressRepositoryMock extends GenerateUniqueIdMock implements AdressRepository{

	private Map<Long, Adress> adresses;
	
	public AdressRepositoryMock() {
		adresses = new HashMap<Long, Adress>();
	}
	
	@Override
	public long createAdress(Adress adress) {
		adress.setId(generateUniqueId());
		adresses.put(adress.getId(), adress);
		return adress.getId();
	}

	@Override
	public List<Adress> getAdresses() {
		List<Adress> adressesTemp = new ArrayList<Adress>();
		adressesTemp.addAll(adresses.values());
		return adressesTemp;
	}
	
	@Autowired(required=false)
	@Qualifier("adressMock")
	public void setAdresses(ArrayList<Adress> adressList) {
		Map<Long, Adress> adresses = new HashMap<Long, Adress>();
		
		for(Adress adress : adressList) {
			adresses.put(adress.getId(), adress);
		}
		
		this.adresses = adresses;
	}

	@Override
	public Adress getAdressById(long id) {
		return adresses.get(id);
	}

	@Override
	public long updateAdress(Adress adress) {
		if(adresses.containsKey(adress.getId())) {
			adresses.put(adress.getId(), adress);
			return adress.getId();
		}
		return -1;
	}

	@Override
	public boolean removeAdressById(long id) {
		return adresses.remove(id) != null ? true : false;
	}

	@Override
	public void clearAllAdresses() {
		adresses.clear();
	}

}
