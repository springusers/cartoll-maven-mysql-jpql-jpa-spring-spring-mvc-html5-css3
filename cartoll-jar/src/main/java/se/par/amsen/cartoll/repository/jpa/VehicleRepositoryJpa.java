package se.par.amsen.cartoll.repository.jpa;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import se.par.amsen.cartoll.domain.vehicle.Vehicle;
import se.par.amsen.cartoll.repository.VehicleRepository;

public class VehicleRepositoryJpa extends AbstractRepositoryJpa implements VehicleRepository {

	@Transactional
	@Override
	public long createVehicle(Vehicle vehicle) {
		em.persist(vehicle);
		return vehicle.getId();
	}

	@Transactional
	@Override
	public long updateVehicle(Vehicle vehicle) {
		return em.merge(vehicle).getId();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Vehicle> getVehicles() {
		return em.createNamedQuery("getAllVehicles").getResultList();
	}

	@Override
	public Vehicle getVehicleById(long id) {
		return em.find(Vehicle.class, id);
	}

	@Transactional
	@Override
	public boolean removeVehicleById(long id) {
		em.remove(em.find(Vehicle.class, id));
		return em.find(Vehicle.class, id) == null;
	}

	@Transactional
	@Override
	public void clearAllVehicles() {
		em.createNamedQuery("clearAllVehicles").executeUpdate();
	}

}
