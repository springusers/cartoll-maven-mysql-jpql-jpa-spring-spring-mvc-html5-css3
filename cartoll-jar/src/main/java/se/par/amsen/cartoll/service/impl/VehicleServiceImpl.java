package se.par.amsen.cartoll.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.par.amsen.cartoll.domain.vehicle.Vehicle;
import se.par.amsen.cartoll.repository.VehicleRepository;
import se.par.amsen.cartoll.service.VehicleService;

@Service
public class VehicleServiceImpl implements VehicleService{

	@Autowired
	private VehicleRepository repository;
	
	@Override
	public long createVehicle(Vehicle vehicle) {
		repository.createVehicle(vehicle);
		return vehicle.getId();
	}

	@Override
	public List<Vehicle> getVehicles() {
		return repository.getVehicles();
	}

	@Override
	public Vehicle getVehicleById(long id) {
		return repository.getVehicleById(id);
	}

	@Override
	public long updateVehicle(Vehicle vehicle) {
		repository.updateVehicle(vehicle);
		return vehicle.getId();
	}

	@Override
	public boolean removeVehicle(long id) {
		return repository.removeVehicleById(id);
	}

	@Override
	public void clearAllVehicles() {
		repository.clearAllVehicles();
	}
}
