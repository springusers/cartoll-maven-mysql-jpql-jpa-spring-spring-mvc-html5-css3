package se.par.amsen.cartoll.service;

import java.util.List;

import se.par.amsen.cartoll.domain.Owner;

public interface OwnerService {
	public long createOwner(Owner owner);
	public List<Owner> getOwners();
	public Owner getOwnerById(long id);
	public long updateOwner(Owner owner);
	public boolean removeOwner(long id);
	public void clearAllOwners();
} 
