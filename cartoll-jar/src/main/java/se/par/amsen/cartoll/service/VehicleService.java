package se.par.amsen.cartoll.service;

import java.util.List;

import se.par.amsen.cartoll.domain.vehicle.Vehicle;

public interface VehicleService {
	public long createVehicle(Vehicle vehicle);
	public long updateVehicle(Vehicle vehicle);
	public List<Vehicle> getVehicles();
	public Vehicle getVehicleById(long id);
	public boolean removeVehicle(long id);
	public void clearAllVehicles();
}
