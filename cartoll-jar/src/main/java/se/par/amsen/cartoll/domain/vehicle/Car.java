package se.par.amsen.cartoll.domain.vehicle;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import se.par.amsen.cartoll.domain.Owner;
import se.par.amsen.cartoll.domain.TaxLevel;

/**
 * Car specifies tax calculations specific for cars
 * @author Pär
 *
 */
@Entity
@DiscriminatorValue(value="Car")
public class Car extends Vehicle {
	public Car(String regNumber, Owner owner) {
		super(regNumber, owner);
	}
	
	public Car() { }

	@Override
	public int calculateTax(TaxLevel taxLevel) {
		return getTaxByLevel(taxLevel);
	}
	
	@Override
	public String getDetailedType() {
		return "Car";
	}
	
	@Override
	public String getDetailedName() {
		return String.format("Car | %s", getRegNumber());
	}

	@Override
	public int getLowTax() {
		return 8;
	}

	@Override
	public int getMediumTax() {
		return 13;
	}

	@Override
	public int getHighTax() {
		return 18;
	}

}
