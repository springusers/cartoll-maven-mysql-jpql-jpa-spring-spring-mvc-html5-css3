<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<header>
	<div class="header">
		<div id="headerContent">
			<div id="logo">
				<h1>
					ZE~ <br>CARTOLL
				</h1>
			</div>
			<div id="navigator">
				<ul>
					<li><div id="separator"></div><a href="<c:url value="/index.html"/>">HOME</a></li>
					<li><div id="separatorAdd">+</div><a href="<c:url value="/addPassage.html"/>">ADD PASSAGE</a></li>
					<li><div id="separatorAdd">-</div><a href="<c:url value="/clearPassages.html?clear=true"/>">CLEAR PASSAGES</a></li>
				</ul>
				<div id="clear"></div>
			</div>
		</div>
	</div>
</header>