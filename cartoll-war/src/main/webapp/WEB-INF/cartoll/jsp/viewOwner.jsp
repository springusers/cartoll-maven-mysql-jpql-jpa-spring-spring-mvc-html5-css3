<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="<c:url value="/resources/styles/reset.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/styles/main.css" />"
	rel="stylesheet">
<title>View Owner</title>
</head>
<body>
	<div class="container">
		<jsp:include page="header.jsp"></jsp:include>
		<div class="content">
			<div id="stations">
				<div id="stationsList">
					<c:choose>
						<c:when test="${empty owner}">
							<p>Could not show owner</p>
						</c:when>
						<c:otherwise>
						<h1>${owner.fullName}</h1>
							<table class="stationTable">
								<tr>
									<th>Reg. No.</th>
									<th>Type</th>
									<th>Number of passages</th>
									<th>Total tax</th>
								</tr>
								<c:forEach items="${vehicleBeans}" var="vehicleBean">
									<tr>
										<td>${vehicleBean.vehicle.regNumber}</td>
										<td>${vehicleBean.vehicle.detailedType}</td>
										<td>${vehicleBean.statistic.passages}</td>
										<td>${vehicleBean.statistic.tax}</td>
									</tr>
								</c:forEach>
							</table>

						</c:otherwise>
					</c:choose>
				</div>
				<div id="stationsStatistics">
					<h1>Adress</h1>
					<p>Street:</p><span>${owner.adress.street}</span>
					<p>Postal code:</p><span>${owner.adress.postalCode}</span>
					<p>City:</p><span>${owner.adress.city}</span>
				</div>
			</div>
			<div id="rightContent"></div>
		</div>
	</div>
</body>
</html>