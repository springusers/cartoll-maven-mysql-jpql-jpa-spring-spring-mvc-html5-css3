package se.par.amsen.cartoll.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.par.amsen.cartoll.service.PassageService;
import se.par.amsen.cartoll.service.TollStationService;

@Controller
@RequestMapping("/viewTollStation/{tollStationId}")
public class ViewTollStationController {

	@Autowired
	TollStationService tollStationService;
	
	@Autowired
	PassageService passageService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView viewStation(@PathVariable long tollStationId) {
		ModelAndView mav = new ModelAndView("viewTollStation");
		mav.addObject("station", tollStationService.getTollStationById(tollStationId));
		mav.addObject("passages", passageService.getPassagesForTollStation(tollStationId));
		return mav;
	}
}